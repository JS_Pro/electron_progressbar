const {app, window} = require('electron');
const ProgressBar = require('electron-progressbar');
const uploadCmd = __dirname + '//arduino//arduino_debug --board arduino:avr:uno --upload ' + __dirname + '//test//test.ino';
const exec = require('child_process').exec;
const dialog = require('electron').dialog;

app.once('ready', function () {
    var progressBar = new ProgressBar({
        indeterminate: true,
        text: 'Uploading sketch...',
        detail: ''
    });

    progressBar
        .on('completed', function () {
            console.info('completed...');
            progressBar.detail = 'Upload completed.';
        })
        .on('aborted', function (value) {
            console.info(`aborted... ${value}`);
        })
        .on('progress', function (value) {
            progressBar.detail = `${value}`;
        });

    setInterval(function () {
        if (!progressBar.isCompleted()) {
            progressBar.value += 1;
        }
    }, 100);

    exec(uploadCmd, (err, stdout, stderr) => {
        if (err) {
            dialog.showMessageBox({
                type: 'info',
                buttons: ['OK', 'Cancel'],
                message: 'Failed'
            });
        } else {
            dialog.showMessageBox({
                type: 'info',
                buttons: ['OK', 'Cancel'],
                message: 'Success'
            });
        }
        progressBar.close();
    });
});